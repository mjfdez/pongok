﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTwo : MonoBehaviour
{

    private Transform playerTransform;
    private Vector3 newPos;
    private Vector3 actualpos;
    public Transform ballPosition;

    public float speed;
    private float move;

    void Start()
    {
        // Equivalente a playerTransform = GetComponent<Transform> ();
        playerTransform = transform;

        newPos = playerTransform.position;
    }

    void Update()
    {
        actualpos = transform.position;
        if (ballPosition.position.y<0)
        { 
            if (ballPosition.position.x > transform.position.x)
            {
            move = 1;
            }
            else
            {  
            move = -1;
            }
        }

        move *= Time.deltaTime;
        move *= speed;
        transform.Translate(move, 0, 0);

        if (transform.position.x > 8f)
        {
            actualpos.x = 8f;
            transform.position = actualpos;
        }
        else if (transform.position.x < -8f)
        {
            actualpos.x = -8f;
            transform.position =actualpos;
        }
    }
}
